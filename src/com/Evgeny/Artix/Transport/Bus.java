package com.Evgeny.Artix.Transport;

public class Bus extends Transport {

    private boolean informationTable;

    /**
     * Конструктор класса автобус
     * @param name название транспортного средства
     * @param numberWheel количество колес
     * @param color цвет транспортного средства
     * @param maxSpeed максимальная скорость транспортного средства
     * @param informationTable присутствует ли информационное табло
     */
    public Bus(String name, int numberWheel, String color, int maxSpeed, boolean informationTable) {
        super(name, numberWheel, color, maxSpeed);
        this.informationTable = informationTable;
    }

    public Bus(String name) {
        super(name);
        this.informationTable = false;

    }

    public String isInformationTable() {

        if (informationTable)
            return "входит в комплектацию";
        else
            return "не входит в комплектацию";
    }

    public void setInformationTable(boolean informationTable) {
        this.informationTable = informationTable;
    }

    @Override
    public void Info() {
        super.Info();
        System.out.println("Информационное табло: "+isInformationTable());
        System.out.println("--------------------------------");
    }
}
