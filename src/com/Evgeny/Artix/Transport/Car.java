package com.Evgeny.Artix.Transport;

public class Car extends Transport {
    public static final String TYPE = "кабриолет";
    private String roofType;

    /**
     * Конструктор класса Автомобиль
     * @param name название транспортного средства
     * @param numberWheel количество колес
     * @param color цвет транспортного средства
     * @param maxSpeed максимальная скорость транспортного средства
     * @param roofType тип крыши ( откидная, кабриолет, обыная )
     */
    public Car(String name, int numberWheel, String color, int maxSpeed, String roofType) {
        super(name, numberWheel, color, maxSpeed);
        this.roofType = roofType;
    }

    public Car() {
        this.roofType = TYPE;
    }

    @Override
    public void Info() {
        super.Info();
        System.out.println("Тип крыши: "+roofType);
        System.out.println("--------------------------------");
    }
}
