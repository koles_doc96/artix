package com.Evgeny.Artix.Transport;

public class KickScooter extends  Transport {

    public static final String CHILDREN = "Детский";
    public static final String ADULTS = "Взрослый";
    private boolean childrenOrAdults;

    /**
     * Конструктор класса самокат
     * @param name название транспортного средства
     * @param numberWheel количество колес
     * @param color цвет транспортного средства
     * @param maxSpeed максимальная скорость транспортного средства
     * @param isChildrenOrAdults возрастные ограничения
     */
    public KickScooter(String name, int numberWheel, String color, int maxSpeed, boolean isChildrenOrAdults) {
        super(name, numberWheel, color, maxSpeed);
        this.childrenOrAdults = isChildrenOrAdults;
    }

    public KickScooter(String name) {
        super(name);
        this.childrenOrAdults = true;
    }

    public String isChildrenOrAdults() {

        if(childrenOrAdults)
            return CHILDREN;
        else
            return ADULTS;
    }

    @Override
    public void Info() {
        super.Info();
        System.out.println("Возрастные ограничения: "+isChildrenOrAdults());
        System.out.println("--------------------------------");
    }

    public void setChildrenOrAdults(boolean childrenOrAdults) {
        this.childrenOrAdults = childrenOrAdults;
    }
}
