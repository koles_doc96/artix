package com.Evgeny.Artix.Transport;

public class Motorcycle extends Transport {

    public static final String TYPE_OF_FINAL_DRIVE = "редуктор";
    /**
     *  Ременная, Цепь, Редуктор
     */
    private String  typeOfFinalDrive;

    /**
     * Конструктор класса мотоцикл
     * @param name название транспортного средства
     * @param numberWheel количество колес
     * @param color цвет транспортного средства
     * @param maxSpeed максимальная скорость транспортного средства
     * @param typeOfFinalDrive тип главной передачи
     */
    public Motorcycle(String name, int numberWheel, String color, int maxSpeed, String typeOfFinalDrive) {
        super(name, numberWheel, color, maxSpeed);
        this.typeOfFinalDrive = typeOfFinalDrive;
    }

    public Motorcycle(String name) {
        super(name);
        this.typeOfFinalDrive = TYPE_OF_FINAL_DRIVE;
    }

    public String getTypeOfFinalDrive() {
        return typeOfFinalDrive;
    }

    public void setTypeOfFinalDrive(String typeOfFinalDrive) {
        this.typeOfFinalDrive = typeOfFinalDrive;
    }

    @Override
    public void Info() {
        super.Info();
        System.out.println("Тип главной передачи: "+typeOfFinalDrive);
        System.out.println("--------------------------------");
    }
}
