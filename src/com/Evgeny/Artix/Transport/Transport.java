package com.Evgeny.Artix.Transport;

public class Transport implements ITransport{

    public static final int NUMBER_WHEEL = 4;
    public static final int MAX_SPEED = 180;
    public static final String COLORBLACK = "Черный";
    public static final String NAME = "Автомобиль";
    public static final String ERROR_MESSAGE_COUNT_WHEEL = "Не корректное число колес";
    private String name;
    private int numberWheel;
    private String color;
    private int maxSpeed;


    /**
     * Конструктор класса транспорт
     * @param name название транспортного средства
     * @param numberWheel количество колес
     * @param color цвет транспортного средства
     * @param maxSpeed максимальная скорость транспортного средства
     */
    public Transport(String name, int numberWheel, String color, int maxSpeed) {
        this.name = name;

        if(numberWheel<=1)
            System.out.println(ERROR_MESSAGE_COUNT_WHEEL);
        else
            this.numberWheel = numberWheel;
        this.color = color;
        this.maxSpeed = maxSpeed;
    }

    public Transport(String name) {
        this.name = name;
    }

    public Transport() {
        this.name = NAME;
        this.numberWheel = NUMBER_WHEEL;
        this.color = COLORBLACK;
        this.maxSpeed = MAX_SPEED;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberWheel() {
        return numberWheel;
    }

    public void setNumberWheel(int numberWheel) {
        this.numberWheel = numberWheel;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public void Info() {

        StringBuilder info= new StringBuilder();

        info.append("Наименование транспорта: "+name+"\n");
        info.append("Кол-во колес: "+ numberWheel+"\n");
        info.append("Цвет: "+color+"\n");
        info.append("Максимальная скорость: "+ maxSpeed);

        System.out.println("--------------------------------");
        System.out.println(info);

    }
}
