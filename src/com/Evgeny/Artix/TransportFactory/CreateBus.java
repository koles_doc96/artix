package com.Evgeny.Artix.TransportFactory;

import com.Evgeny.Artix.Transport.Bus;
import com.Evgeny.Artix.Transport.Transport;

public class CreateBus extends TypeTransport {

    @Override
    public Transport createTransport() {
        return new Bus("Автобус");
    }
}
