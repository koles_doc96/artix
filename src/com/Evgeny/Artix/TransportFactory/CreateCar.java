package com.Evgeny.Artix.TransportFactory;

import com.Evgeny.Artix.Transport.Car;
import com.Evgeny.Artix.Transport.Transport;

public class CreateCar extends TypeTransport {
    @Override
    public Transport createTransport() {
        return new Car();
    }
}
