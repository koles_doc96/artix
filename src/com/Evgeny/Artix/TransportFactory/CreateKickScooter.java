package com.Evgeny.Artix.TransportFactory;

import com.Evgeny.Artix.Transport.KickScooter;
import com.Evgeny.Artix.Transport.Transport;

public class CreateKickScooter extends TypeTransport {
    @Override
    public Transport createTransport() {
        return new KickScooter("Самокат");
    }
}
