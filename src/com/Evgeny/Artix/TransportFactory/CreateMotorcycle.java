package com.Evgeny.Artix.TransportFactory;

import com.Evgeny.Artix.Transport.Motorcycle;
import com.Evgeny.Artix.Transport.Transport;

public class CreateMotorcycle extends TypeTransport {
    @Override
    public Transport createTransport() {
        return new Motorcycle("Мотоцикл");
    }
}
