package com.Evgeny.Artix.TransportFactory;

import com.Evgeny.Artix.Transport.Transport;

public abstract class TypeTransport {

    /**
     * Создаем транспортное средство
     * @return транспортное средство
     */
    public void create()
    {
        Transport transport = createTransport();
        transport.Info();
    }
    public  abstract Transport createTransport();
}
